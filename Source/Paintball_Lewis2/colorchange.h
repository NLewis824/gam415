// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "colorchange.generated.h"

class UMaterialInstanceDynamic;

UCLASS()
class PAINTBALL_LEWIS2_API Acolorchange : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Acolorchange();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UMaterialInstanceDynamic* DynamicMaterial;

};
