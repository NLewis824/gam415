// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paintball_Lewis2GameMode.generated.h"

UCLASS(minimalapi)
class APaintball_Lewis2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintball_Lewis2GameMode();
};



