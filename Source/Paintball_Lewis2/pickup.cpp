// Fill out your copyright notice in the Description page of Project Settings.


#include "pickup.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

// Sets default values
Apickup::Apickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyObject = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Object"));
	MyObject->SetSimulatePhysics(true);
	RootComponent = MyObject;

	bHolding = false;
	bGravity = true;
}

// Called when the game starts or when spawned
void Apickup::BeginPlay()
{
	Super::BeginPlay();

	MyCharacter = UGameplayStatics::GetPlayerCharacter(this, 0);
	PlayerCamera = MyCharacter->FindComponentByClass<UCameraComponent>();



	TArray<USceneComponent*> Components;
	MyCharacter->GetComponents(Components);
	

	if (Components.Num() > 0)
	{
		for (auto& Comp : Components)
		{
			if (Comp->GetName() == "HoldingComponent")
			{
				HoldingComp = Cast<USceneComponent>(Comp);
			}
		}
	}
	
}

// Called every frame
void Apickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bHolding && HoldingComp)
	{
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation(), HoldingComp->GetComponentRotation());
	}

}

void Apickup::RotateActor()
{
	ControlRotation = GetWorld()->GetFirstPlayerController()->GetControlRotation();
	SetActorRotation(FQuat(ControlRotation));
}

void Apickup::Take()
{
	bHolding = !bHolding;
	bGravity = !bGravity;
	MyObject->SetEnableGravity(bGravity);
	MyObject->SetSimulatePhysics(bHolding ? false : true);
	MyObject->SetCollisionEnabled(bHolding ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);


	if (!bHolding)
	{
		ForwardVector = PlayerCamera->GetForwardVector();
		MyObject->AddForce(ForwardVector * 100000 * MyObject->GetMass());

	}
}




