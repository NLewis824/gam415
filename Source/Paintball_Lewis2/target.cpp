// Fill out your copyright notice in the Description page of Project Settings.


#include "target.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"



// Sets default values
Atarget::Atarget()
{
	PrimaryActorTick.bCanEverTick = true;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Mesh"));
	RootComponent = MyMesh;

	MyBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BOX COMP"));
	MyBoxComponent->InitBoxExtent(FVector(100, 100, 100));
	MyBoxComponent->SetCollisionProfileName("Trigger");

	MyBoxComponent->SetupAttachment(RootComponent);




	OnMaterial = CreateDefaultSubobject<UMaterial>(TEXT("On Material"));
	OffMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Off Material"));

	MyBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &Atarget::OnOverlapBegin);

}

// Called when the game starts or when spawned
void Atarget::BeginPlay()
{
	Super::BeginPlay();

	DrawDebugBox(GetWorld(), GetActorLocation(), FVector(100, 100, 100), FColor::Black, true, 0, 0, 0);

	MyMesh->SetMaterial(0, OffMaterial);

	
}

// Called every frame
void Atarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Atarget::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		MyMesh->SetMaterial(0, OnMaterial);
	}
}