// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "target.generated.h"


UCLASS()
class PAINTBALL_LEWIS2_API Atarget : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Atarget();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* MyMesh;

	UPROPERTY(EditAnywhere)
		class UMaterial* OffMaterial;

	UPROPERTY(EditAnywhere)
		class UMaterial* OnMaterial;


	UPROPERTY()
		class UBoxComponent* MyBoxComponent;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


};
