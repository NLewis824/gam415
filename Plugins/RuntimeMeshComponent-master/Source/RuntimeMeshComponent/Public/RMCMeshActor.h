// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RuntimeMeshActor.h"
#include "RMCMeshActor.generated.h"

/**
 * 
 */
UCLASS()
class RUNTIMEMESHCOMPONENT_API ARMCMeshActor : public ARuntimeMeshActor
{
	GENERATED_BODY()
	
};
